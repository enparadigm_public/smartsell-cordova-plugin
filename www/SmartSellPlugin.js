var exec = cordova.require('cordova/exec');
var PLUGIN_NAME = "SmartSellPlugin";
var SmartSellPlugin = function() {
    console.log('SmartSellPlugin instanced');
};

SmartSellPlugin.prototype.initialise = function(baseUrl,mobile,name,email,designation,language,userType,accessToken,refreshToken,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'initialise', [baseUrl,mobile,name,email,designation,language,userType,accessToken,refreshToken]);
};

SmartSellPlugin.prototype.isUserInitialised = function(mobile,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'isUserInitialised', [mobile]);
};

SmartSellPlugin.prototype.validateUser = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed validateUser');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'validateUser', argument);
};

SmartSellPlugin.prototype.openHomePage = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed openHomePage');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'openHomePage', argument);
};

SmartSellPlugin.prototype.updateToken = function(accessToken,refreshToken,mobile,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    console.log('Executed updateToken');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'updateToken', [accessToken,refreshToken,mobile]);
};

if (typeof module != 'undefined' && module.exports) {
    module.exports = SmartSellPlugin;
}