


# Smartsell Cordova Plugin

## Pre-requirement
Our SDK uses AndroidX libraries in its core. So you need to migrate your app to AndroidX by installing `cordova-plugin-androidx` plugin.

## Installation
```sh
$ cd CORDOVA_PROJECT
$ cordova plugin add https://gitlab.com/enparadigm_public/smartsell-cordova-plugin
```
**Note:** The plugin which we are using is present in private repository. So you need to add following lines in your gradle.properties file.
```
artifactory_repo_smartsell_release_url=http://artifactory.enparadigm.com/artifactory/smartsell_release
artifactory_username=USERNAME_GIVEN_BY_US
artifactory_password=PASSWORD_GIVEN_BY_US
```


### Handling Push-Notification (Optional)
Our Library can handle the push notification for you. To make it happen we need to add following in your ``onMessageReceived`` function of ``FirebaseMessagingService``
```kotlin
@Override  
public void onMessageReceived(RemoteMessage remoteMessage) {  
  super.onMessageReceived(remoteMessage);  
  if (SmartSell.isSmartSellNotification(remoteMessage)) {
    SmartSell.handleFcmNotification(this, remoteMessage, MainActivity.class); // MainActivity will be opened while closing our sdk
  }
}
```

Also make sure to initialize our library in your Application class. If we are not calling init in your Application class, then the app will crash on receving push Notification.
```kotlin
// Import these packages  
import com.enparadigm.smartsell.SmartSell;  
import com.enparadigm.smartsell.SmartSellConfig;


@Override  
public void onCreate() {  
  //Add this code in your Application class
  //-------START----------  
  SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(this);  
  if (pluginConfig.base_url != null && pluginConfig.base_url.length() > 10) {  
  SmartSell.isUserInitialised(getApplicationContext(), pluginConfig.mobile, new InitCallback() {  
        @Override  
        public void onSuccess() {  }  
        @Override  
        public void onError(Throwable throwable) {  }  
    });  
  }
  //-------END-----------
  super.onCreate();  
}
```

## Plugin functions
There are  2 functions in the plugin,
 1. **isUserInitialised** will validate SDK has all required data. If the data present then it will open the SDK.
 2. **initialise** will initialise the plugin with given data and open the SDK.
 
**Note:** You can skip calling **isUserInitialised** for the first time, as the SDK will not have required data untill we do **initialise**. Next time onword you can call **isUserInitialised** to open the SDK. 


## Flow Changes
Previous version of our plugin had 3 function namely “init” , “validateUser” and “openHome”. To open our plugin we where calling those function in the order of “init” -> “validateUser” -> “openHome”.

In this version of our library we moved token generation part to the parent app for security reasons. Also we reduced some method calls for simplicity/performance. Now we have only 2 function in our plugin.
1. isUserInitialised -> will check user data on the plugin. if available it will return success and open our SDK.
2. initialise -> this will initialise our SDK and open it. For this we need to pass the tokens which we need to retrieve from the parent app by calling our “login” API

Now the flow of the call will be isUserInitialised -> initialise (Only when isUserInitialised returns error) . So only for the first time we do initialise. next time on-words isUserInitialised will open the app directly.


### Sample code to call plugin function
```javascript
// We don't need to start the SDK. if initialisation is succes then it will open the SDK.
// Make sure to pass same mobile number for isUserInitialised and initialise functions
function isUserInitialised() {
    var plugin = new SmartSellPlugin();
    // Here 9999999999 is mobile number
    plugin.isUserInitialised("9999999999",function (msg) {
    console.log(msg) // Success Callback. 
    },
        function (err) {
            console.log(err); 
            /*First time we get this error , because we need to init the SDK atlease once. 
            If you get error then, your app need to get new AccessToken and RefreshToken and pass it to the SDK. 
            To pass new tokens use initialise Function. 
            Next time onwords if you call this function it should return success and open the SDK. */
        });
}

/* Make sure to pass the parameter in the following order "API_URL_FOR_PLUGIN,MOBILE_NUMBER,NAME,EMAIL,DESIGNATION,LANGUAGE_SHORT_FORM,USER_TYPE,ACCESS_TOKEN,REFRESH_TOKEN" for this function
  API_URL_FOR_PLUGIN - URL to which the plugin should connect
  LANGUAGE_SHORT_FORM[send "en" for English] - plugin langugae.  eg: If you want plugin in Hindi language then send "hi"
*/
function initialise() {
  var smartSellPlugin = new SmartSellPlugin();
  smartSellPlugin.initialise("http://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/","9999999999","UsersName","email@mail.com","Developer","en","1","access token", "refresh token",function (msg) {
              console.log(msg);
        },
        function (err) {
              console.log(err);
        });
}
```
