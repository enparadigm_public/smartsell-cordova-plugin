package com.enparadigm.smartsellplugin;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.enparadigm.smartsell.sdk.InitCallback;
import com.enparadigm.smartsell.sdk.LoginTask;

import com.enparadigm.smartsell.sdk.SmartSell;
import com.enparadigm.smartsell.sdk.SmartSellConfig;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

import io.flutter.plugin.common.MethodChannel;


/**
 * This class echoes a string called from JavaScript.
 */
public class SmartSellPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initialise") && args.length() == 9) {
            initSmartSell(args, callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("isUserInitialised")) {
            isUserInitialised(args.getString(0), callbackContext);
            Log.d("SmartSellPlugin", "executed isUserInitialised");
            return true;
        } else if (action.equalsIgnoreCase("validateUser")) {
            validateUser(callbackContext);
            Log.d("SmartSellPlugin", "executed validateUser");
            return true;
        } else if (action.equalsIgnoreCase("updateToken") && args.length() == 2) {
            updateToken(args, callbackContext);
            Log.d("SmartSellPlugin", "executed updateToken");
            return true;
        } else if (action.equalsIgnoreCase("openHomePage")) {
            openHomePage(callbackContext);
            Log.d("SmartSellPlugin", "executed openHomePage");
            return true;
        }
        return false;
    }

    private void initSmartSell(JSONArray args, CallbackContext callbackContext) {
        try {

            Context context = cordova.getContext();
            SmartSellPluginConfig.setBaseURL(context, args.getString(0));
            SmartSellPluginConfig.setNumber(context, args.getString(1));
            SmartSellPluginConfig.setName(context, args.getString(2));
            SmartSellPluginConfig.setEmail(context, args.getString(3));
            SmartSellPluginConfig.setDesignation(context, args.getString(4));
            SmartSellPluginConfig.setLanguage(context, args.getString(5));
            SmartSellPluginConfig.setUserType(context, args.getString(6));
            SmartSellPluginConfig.setAccessToken(context, args.getString(7));
            SmartSellPluginConfig.setRefreshToken(context, args.getString(8));

            SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(context);
            SmartSellConfig config = SmartSellConfig.getInstance();
            config.setBaseUrl(pluginConfig.base_url);
            config.setSignatureFromUserData(pluginConfig.name, pluginConfig.designation, pluginConfig.email, pluginConfig.mobile);
            config.setLanguage(pluginConfig.language);
            config.setAccessToken(pluginConfig.accessToken);
            config.setRefreshToken(pluginConfig.refreshToken);
            config.setUserTypeId(pluginConfig.userType);
            SmartSell.init(config, new InitCallback() {
                @Override
                public void onSuccess() {
                    SmartSell.open(context);
                    callbackContext.success("Success");
                }

                @Override
                public void onError(Throwable throwable) {
                    callbackContext.error("Failed : " + throwable.getMessage());
                }
            });
        } catch (Exception e) {
            if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                callbackContext.success("Success");
            } else {
                callbackContext.error("Failed : " + e.getMessage());
            }
        }
    }

    private void isUserInitialised(String mobileNumber, CallbackContext callbackContext) {
        try {

            Context context = cordova.getContext();
            SmartSell.isUserInitialised(context, mobileNumber, new InitCallback() {
                @Override
                public void onSuccess() {
                    SmartSell.open(context);
                    callbackContext.success("Success");
                }

                @Override
                public void onError(Throwable throwable) {
                    callbackContext.error("Failed : " + throwable.getMessage());
                }
            });
        } catch (Exception e) {
            if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                callbackContext.success("Success");
            } else {
                callbackContext.error("Failed : " + e.getMessage());
            }
        }
    }

    private void validateUser(CallbackContext callbackContext) {
        SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(cordova.getActivity().getApplication());
        SmartSell.validateUser(pluginConfig.mobile, pluginConfig.userType, new LoginTask() {
            @Override
            public void success() {
                callbackContext.success("Success");
            }

            @Override
            public void failure(Throwable error) {
                callbackContext.error("Failed : " + error.getMessage());
            }
        });
    }

    private void updateToken(JSONArray args, CallbackContext callbackContext) {
        try {
            String accessToken = args.getString(0);
            String refreshToken = args.getString(1);
            String username = args.getString(2);


            SmartSell.updateToken(accessToken, refreshToken, username, new LoginTask() {
                @Override
                public void success() {
                    callbackContext.success("Success");
                }

                @Override
                public void failure(Throwable throwable) {
                    callbackContext.error("Failed : " + throwable.getMessage());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openHomePage(CallbackContext callbackContext) {
        SmartSell.open(cordova.getActivity());
        callbackContext.success("Success");
    }

}
