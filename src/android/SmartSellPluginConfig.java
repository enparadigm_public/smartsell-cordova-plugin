package com.enparadigm.smartsellplugin;

import android.content.Context;
import android.content.SharedPreferences;

public class SmartSellPluginConfig {

    public String base_url, name, email, mobile, designation, userType, language = "en";
    private static SharedPreferences preferences;
    public String accessToken;
    public String refreshToken;
    private static SmartSellPluginConfig instance;

    private static String PREFERENCE_NAME = "NAME";
    private static String PREFERENCE_NUMBER = "NUMBER";
    private static String PREFERENCE_EMAIL = "EMAIL";
    private static String PREFERENCE_DESIGNATION = "DESIGNATION";
    private static String PREFERENCE_USER_TYPE = "USER_TYPE";
    private static String PREFERENCE_BASE_URL = "BASE_URL";
    private static String PREFERENCE_LANGUAGE = "LANGUAGE";
    private static String PREFERENCE_ACCESS_TOKEN = "ACCESS_TOKEN";
    private static String PREFERENCE_REFRESH_TOKEN = "REFRESH_TOKEN";

    private SmartSellPluginConfig(Context ctx) {
        base_url = getBaseURL(ctx);
        name = getName(ctx);
        email = getEmail(ctx);
        mobile = getNumber(ctx);
        designation = getDesignation(ctx);
        name = getName(ctx);
        userType = getUserType(ctx);
        language = getLanguage(ctx);
        accessToken = getAccessToken(ctx);
        refreshToken = getRefreshToken(ctx);
    }


    public static SmartSellPluginConfig getInstance(Context ctx) {
        if (instance == null || instance.base_url.length() < 5) {
            instance = new SmartSellPluginConfig(ctx);
        }
        return instance;
    }

    static SharedPreferences getPreference(Context context) {
        if (preferences == null)
            preferences = context.getSharedPreferences("SmartSellPluginPreference", Context.MODE_PRIVATE);
        return preferences;
    }

    static String getBaseURL(Context context) {
        return getPreference(context).getString(PREFERENCE_BASE_URL, "");
    }

    static void setBaseURL(Context context, String name) {
        getPreference(context).edit().putString(PREFERENCE_BASE_URL, name).apply();
    }

    static String getName(Context context) {
        return getPreference(context).getString(PREFERENCE_NAME, "");
    }

    static void setName(Context context, String name) {
        getPreference(context).edit().putString(PREFERENCE_NAME, name).apply();
    }

    static String getNumber(Context context) {
        return getPreference(context).getString(PREFERENCE_NUMBER, "");
    }

    static void setNumber(Context context, String number) {
        getPreference(context).edit().putString(PREFERENCE_NUMBER, number).apply();
    }

    static String getDesignation(Context context) {
        return getPreference(context).getString(PREFERENCE_DESIGNATION, "");
    }

    static void setDesignation(Context context, String designation) {
        getPreference(context).edit().putString(PREFERENCE_DESIGNATION, designation).apply();
    }

    static String getEmail(Context context) {
        return getPreference(context).getString(PREFERENCE_EMAIL, "");
    }

    static void setEmail(Context context, String email) {
        getPreference(context).edit().putString(PREFERENCE_EMAIL, email).apply();
    }

    static String getLanguage(Context context) {
        return getPreference(context).getString(PREFERENCE_LANGUAGE, "");
    }

    static void setLanguage(Context context, String language) {
        getPreference(context).edit().putString(PREFERENCE_LANGUAGE, language).apply();
    }

    static String getUserType(Context context) {
        return getPreference(context).getString(PREFERENCE_USER_TYPE, "");
    }

    static void setUserType(Context context, String userType) {
        getPreference(context).edit().putString(PREFERENCE_USER_TYPE, userType).apply();
    }

    static String getAccessToken(Context context) {
        return getPreference(context).getString(PREFERENCE_ACCESS_TOKEN, "");
    }

    static void setAccessToken(Context context, String userType) {
        getPreference(context).edit().putString(PREFERENCE_ACCESS_TOKEN, userType).apply();
    }

    static String getRefreshToken(Context context) {
        return getPreference(context).getString(PREFERENCE_REFRESH_TOKEN, "");
    }

    static void setRefreshToken(Context context, String userType) {
        getPreference(context).edit().putString(PREFERENCE_REFRESH_TOKEN, userType).apply();
    }

}